$(function () {
	$('#datepicker, #datepicker2').datepicker({format: "yyyy/mm/dd", autoclose: true});

	
	
	
	var $tableSel = $('#example1');
	  
  	$('#btnFiltrar').on('click', function(e){
    	e.preventDefault();
    	var startDate 	= $('#datepicker').val(),
        	endDate 	= $('#datepicker2').val(),
        	opcionFecha	= $('#opcionFecha').val();
    
    	filterByDate(opcionFecha, startDate, endDate);    
    	$tableSel.dataTable().fnDraw();
  	});
	  
  	$('#btnLimpiar').on('click', function(e){
    	e.preventDefault();
    	$.fn.dataTableExt.afnFiltering.length = 0;
    	$tableSel.dataTable().fnDraw();
    	$('#datepicker, #datepicker2').val('');
  	});
	  
	var filterByDate = function(column, startDate, endDate) {
		// Custom filter syntax requires pushing the new filter to the global filter array
		$.fn.dataTableExt.afnFiltering.push (function( oSettings, aData, iDataIndex ) {
			var rowDate = normalizeDate(aData[column]),
	            start = normalizeDate(startDate),
	            end = normalizeDate(endDate);
	          
	        // If our date from the row is between the start and end
	        if (start <= rowDate && rowDate <= end) {
	           return true;
	        } else if (rowDate >= start && end === '' && start !== ''){
	           return true;
	        } else if (rowDate <= end && start === '' && end !== ''){
	           return true;
	        } else {
	           return false;
	        }

	    });
	};

	// converts date strings to a Date object, then normalized into a YYYYMMMDD format (ex: 20131220). 
	// Makes comparing dates easier. ex: 20131220 > 20121220
	var normalizeDate = function(dateString) {
	  	var date = new Date(dateString);
	  	var normalized = date.getFullYear() + '' + (("0" + (date.getMonth() + 1)).slice(-2)) + '' + ("0" + date.getDate()).slice(-2);
	  	return normalized;
	}

	$('.btnEstado').click(function()
	{
		var contexto	= $(this),
			idRenta	= contexto.data('idrenta'),
			estado 	= contexto.parents('.fila').find('.estado').val(),
			tdEstado = $('.td-estado');

		$.post('/rentas/estado', {idrenta: idRenta, estado: estado}, function(respuesta)
		{
			if(estado==1) {
				contexto.parents('.fila').find('.td-estado').text('abierto');
			}
			else if(estado==2) {
				contexto.parents('.fila').find('.td-estado').text('cerrado');
			}
			else {
				contexto.parents('.fila').find('.td-estado').text('anulado');
			}
		});
	});
});