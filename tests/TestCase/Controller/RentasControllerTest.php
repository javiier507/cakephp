<?php
namespace App\Test\TestCase\Controller;

use App\Controller\RentasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\RentasController Test Case
 */
class RentasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rentas',
        'app.vehiculos',
        'app.clientes'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
