<?php
namespace App\Test\TestCase\Controller;

use App\Controller\VechiculosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\VechiculosController Test Case
 */
class VechiculosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vechiculos'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
