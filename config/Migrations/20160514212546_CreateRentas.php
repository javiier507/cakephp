<?php
use Migrations\AbstractMigration;

class CreateRentas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('rentas');
        
        /*$table->addColumn('vehiculo', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => false,
        ]);
        $table->addColumn('cliente', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => false,
        ]);*/

        $table->addColumn('fecha_salida', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => false,
        ]);
        $table->addColumn('fecha_entrada', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => false,
        ]);
        $table->addColumn('costo', 'float', [
            'default' => 50,
            'null' => false,
        ]);
        $table->addColumn('estado', 'integer', [
            'default' => null,
            'null' => false,
        ]);

        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        
        $table->create();

        $refTable = $this->table('rentas');
        $refTable->addColumn('vehiculo_id', 'integer', array('signed' => 'disable'))
            ->addForeignKey('vehiculo_id', 'vehiculos', 'id', array('delete' => 'CASCADE', 'update' => 'NO_ACTION'))
            ->update();
        $refTable->addColumn('cliente_id', 'integer', array('signed' => 'disable'))
            ->addForeignKey('cliente_id', 'clientes', 'id', array('delete' => 'CASCADE', 'update' => 'NO_ACTION'))
            ->update();
    }
}
