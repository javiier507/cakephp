<?php

use Phinx\Migration\AbstractMigration;

class CreateVehiculosSeed extends AbstractMigration
{
    public function up()
    {
        $faker = \Faker\Factory::create();
        $populator = new Faker\ORM\CakePHP\Populator($faker);

        $populator->addEntity('vehiculos', 20, [
            'chasis' => function() use ($faker) {
                return $faker->ean8;
            },
            'motor' => function() use ($faker) {
                return $faker->ean8;
            },
            'placa' => function() use ($faker) {
                return $faker->numberBetween($min = 10000000, $max = 99999999);
            },
            'color' => function() use ($faker) {
                return $faker->safeColorName;
            },
            'modelo' => function() use ($faker) {
                return '';
            },
            'marca' => function() use ($faker) {
                return $faker->randomElement($array = array ('toyota','nissan','hyundai','honda', 'kia','suzuki','mitsubishi'));
            },
            'kilometraje' => function() use ($faker) {
                return $faker->numberBetween($min = 1000, $max = 50000);
            },
            'tipo' => function() use ($faker) {
                return $faker->numberBetween($min = 1, $max = 5);
            },
            'created' => function () use ($faker) {
                return $faker->dateTimeBetween($startDate = 'now', $endDate = 'now');
            },
            'modified' => function () use ($faker) {
                return $faker->dateTimeBetween($startDate = 'now', $endDate = 'now');
            },
        ]);

        $populator->execute();
    }
}
