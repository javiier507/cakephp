<?php
use Migrations\AbstractMigration;

class CreateVehiculos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('vehiculos');
        
        $table->addColumn('chasis', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => false,
        ]);
        $table->addColumn('motor', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => false,
        ]);
        $table->addColumn('placa', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => false,
        ]);
        $table->addColumn('color', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => false,
        ]);
        $table->addColumn('modelo', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => false,
        ]);
        $table->addColumn('marca', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => false,
        ]);
        $table->addColumn('kilometraje', 'integer', [
            'default' => 0,
            'null' => false,
        ]);
        $table->addColumn('tipo', 'integer', [
            'default' => 1,
            'null' => false,
        ]);
        
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        
        $table->addIndex([
            'chasis',
        ], [
            'name' => 'chasis_index',
            'unique' => true,
        ]);
        $table->addIndex([
            'motor',
        ], [
            'name' => 'motor_index',
            'unique' => true,
        ]);
        $table->addIndex([
            'placa',
        ], [
            'name' => 'placa_index',
            'unique' => true,
        ]);
        
        $table->create();
    }
}
