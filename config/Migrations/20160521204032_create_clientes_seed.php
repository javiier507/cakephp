<?php

use Phinx\Migration\AbstractMigration;

class CreateClientesSeed extends AbstractMigration
{
    public function up()
    {
        $faker = \Faker\Factory::create();
        $populator = new Faker\ORM\CakePHP\Populator($faker);

        $populator->addEntity('clientes', 10, [
            'cedula' => function() use ($faker) {
                return $faker->ean8;
            },
            'licencia' => function() use ($faker) {
                return $faker->numberBetween($min = 10000000, $max = 99999999);
            },
            'tipo_licencia' => function() use ($faker) {
                return $faker->randomElement($array = array ('c','d','e','e1','e2','g','h','i'));
            },
            'nombre' => function () use ($faker) {
                return $faker->firstName;
            },
            'apellido' => function () use ($faker) {
                return $faker->lastName;
            },
            'fecha_nacimiento' => function () use ($faker) {
                return $faker->date($format = 'Y-m-d', $max = 'now');
            },
            'correo' => function() use ($faker) {
                return $faker->email;
            },
            'celular' => function () use ($faker) {
                return $faker->phoneNumber;
            },
            'direccion' => function() use ($faker) {
                return $faker->address;
            },
            'created' => function () use ($faker) {
                return $faker->dateTimeBetween($startDate = 'now', $endDate = 'now');
            },
            'modified' => function () use ($faker) {
                return $faker->dateTimeBetween($startDate = 'now', $endDate = 'now');
            },
        ]);

        $populator->execute();
    }
}
