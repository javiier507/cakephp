<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Renta Entity.
 *
 * @property int $id
 * @property string $fecha_salida
 * @property string $fecha_entrada
 * @property float $costo
 * @property int $estado
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $vehiculo_id
 * @property \App\Model\Entity\Vehiculo $vehiculo
 * @property int $cliente_id
 * @property \App\Model\Entity\Cliente $cliente
 */
class Renta extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
