<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Registro Clientes</h3>
                <p><?= $this->Flash->render(); ?></p>
            </div>
            <form action="/clientes/registro" method="POST" role="form" >
                <div class="box-body">
                    <div class="form-group">
                        <label>Cedula</label>
                        <input type="text" name="cedula" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Licencia</label>
                        <input type="text" name="licencia" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Tipo Licencia</label>
                        <select name="tipo_licencia" class="form-control">
                            <option value="c">C</option>
                            <option value="d">D</option>
                            <option value="e1">E1</option>
                            <option value="e2">E2</option>
                            <option value="f">F</option>
                            <option value="g">G</option>
                            <option value="h">H</option>
                            <option value="i">I</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" name="nombre" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Apellido</label>
                        <input type="text" name="apellido" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Fecha Nacimiento</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="fecha_nacimiento" class="form-control pull-right" id="datepicker">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Correo</label>
                        <input type="email" name="correo" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Celular</label>
                        <input type="text" name="celular" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Direccion</label>
                        <input type="text" name="direccion" class="form-control">
                    </div>
                </div>                
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Registrar</button>
                </div>
            </form>
        </div>
    </div>
</div>