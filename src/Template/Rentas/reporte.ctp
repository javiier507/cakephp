<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				Seleccione el intervalo de fecha
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-xs-12 col-md-2">
						<label>Fecha Inicial</label>
					</div>
					<div class="col-xs-12 col-md-2">
						<div class="input-group date">
						    <div class="input-group-addon">
						        <i class="fa fa-calendar"></i>
						    </div>
						    <input type="text" name="fecha_salida" class="form-control pull-right" id="datepicker">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-2">
						<label>Fecha Final</label>
					</div>
					<div class="col-xs-12 col-md-2">
						<div class="input-group date">
						    <div class="input-group-addon">
						        <i class="fa fa-calendar"></i>
						    </div>
						    <input type="text" name="fecha_entrada" class="form-control pull-right" id="datepicker2">
						</div>
					</div>					
				</div>
				<div class="row" style="margin-top: 10px">
					<div class="col-xs-2 col-md-2">
						<button id="btnFiltrar" class="btn btn-primary">Filtrar</button>
					</div>
					<div class="col-xs-3 col-md-2">
						<select id="opcionFecha" class="form-control">
						    <option value="1">Fecha Salida</option>
						    <option value="2">Fecha Entrada</option>
						</select>
					</div>
					<div class="col-xs-2 col-md-2">
						<button id="btnLimpiar" class="btn btn-primary">Limpiar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <?= $this->element('CustomPartials/rentasTable') ?>   