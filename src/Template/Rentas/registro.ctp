<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Registro de Rentas</h3>
                <p><?= $this->Flash->render(); ?></p>             
            </div>
            <form action="/rentas/registro" method="POST" role="form" >
                <div class="box-body">
                   <div class="form-group">
                        <label>Vehiculo</label>
                        <select name="vehiculo_id" class="form-control">
                            <option value="0">Seleccione un vehiculo</option>
                            <?php foreach( $data[0]['vehiculos'] as $vehiculo ): ?>
                            	<option value="<?php echo $vehiculo['id']; ?>">
                            		<?php echo $vehiculo['modelo']; ?>
                            	</option>
                            <?php endforeach; ?>                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Cliente</label>
                        <select name="cliente_id" class="form-control">
                            <option value="0">Seleccione un cliente</option>
                            <?php foreach( $data[1]['clientes'] as $cliente ): ?>
                            	<option value="<?php echo $cliente->id; ?>">
                            		<?php echo $cliente->fullName; ?>
                            	</option>
                            <?php endforeach; ?>   
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Fecha Salida</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="fecha_salida" class="form-control pull-right" id="datepicker">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Fecha Entrada</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="fecha_entrada" class="form-control pull-right" id="datepicker2">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Costo</label>
                        <input type="text" name="costo" class="form-control">
                    </div>
                   <div class="form-group">
                       <label>Estado</label>
                       <select name="estado" class="form-control">
                           <option value="1">Abierto</option>
                           <option value="2">Cerrado</option>
                           <option value="3">Anulado</option>
                       </select>
                   </div>
                </div>                
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Registrar</button>
                </div>
            </form>
        </div>
    </div>
</div>