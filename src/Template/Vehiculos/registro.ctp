<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Registro Vehiculos</h3>
                <p><?= $this->Flash->render(); ?></p>
            </div>
            <form action="/vehiculos/registro" method="POST" role="form">
                <div class="box-body">
                    <div class="form-group">
                        <label># Chasis</label>
                        <input type="text" name="chasis" class="form-control">
                    </div>
                    <div class="form-group">
                        <label># Motor</label>
                        <input type="text" name="motor" class="form-control">
                    </div>
                    <div class="form-group">
                        <label># Placa</label>
                        <input type="text" name="placa" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Color</label>
                        <select name="color" class="form-control">
                            <option value="aqua">aqua</option>
                            <option value="blue">blue</option>
                            <option value="silver">silver</option>
                            <option value="gray">gray</option>
                            <option value="white">white</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Marca</label>
                        <select name="marca" class="form-control">
                            <option value="0">Seleccione un automovil</option>
                            <?php foreach($vehiculos as $value): ?>
                                <option value="<?php echo $value->marca; ?>">
                                    <?php echo $value->marca; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Modelo</label>
                        <select name="modelo" class="form-control">
                            <option value="Yaris">Yaris</option>
                            <option value="Corolla">Corolla</option>
                            <option value="Hilux">Hilux</option>
                            <option value="Fortuner">Fortuner</option>
                            <option value="Prado">Prado</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label># Kilometraje</label>
                        <input type="text" name="kilometraje" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Tipo</label>
                        <select name="tipo" class="form-control">
                            <option value="1">Sedan</option>
                            <option value="2">4X4</option>
                        </select>
                    </div>
                </div>                
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Registrar</button>
                </div>
            </form>
        </div>
    </div>
</div>