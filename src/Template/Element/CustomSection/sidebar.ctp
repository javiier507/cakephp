<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/adminlte/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Carlos Peñalba</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <li class="active"><a href="/"><i class="fa fa-link"></i> <span>Escritorio</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Vehiculos</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="/vehiculos/registro">Ingreso</a></li>
                    <li><a href="/vehiculos/index">Listado</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Clientes</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="/clientes/registro">Ingreso</a></li>
                    <li><a href="/clientes/index">Listado</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Rentas</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="/rentas/registro">Ingreso</a></li>
                    <li><a href="/rentas/index">Listado</a></li>
                     <li><a href="/rentas/reporte">Reporte</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>