<?php if( !empty($rentas) ): ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Tabla Rentas</h3>
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table id="example1" class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <th>Id</th>
                                            <th>Fecha Salida</th>
                                            <th>Fecha Entrada</th>
                                            <th>Costo</th>
                                            <th>Vehiculo</th>
                                            <th>Cliente</th>
                                            <th>Estado</th>
                                            <th>Actualizar Estado</th>
                                            <th>Confirmar</th>
                                        </thead>
                                        <tbody>
                                            <?php foreach($rentas as $renta): ?>
                                                <tr class="fila">
                                                    <td><?php echo $renta['id']; ?></td>
                                                    <td><?php echo $renta['fecha_salida']; ?></td>
                                                    <td><?php echo $renta['fecha_entrada']; ?></td>
                                                    <td><?php echo $renta['costo']; ?></td>
                                                    <td><?php echo $renta['modelo']; ?></td>
                                                    <td><?php echo $renta['cedula']; ?></td>
                                                    <?php if($renta['estado'] == 1): ?>
                                                        <td class="td-estado">Abierto</td>
                                                    <?php elseif($renta['estado'] == 2): ?>
                                                        <td class="td-estado">Cerrado</td>
                                                    <?php else: ?>
                                                        <td class="td-estado">Anulado</td>
                                                    <?php endif; ?>
                                                    <td>
                                                        <select class="form-control estado">
                                                            <option value="1">Abierto</option>
                                                            <option value="2">Cerrado</option>
                                                            <option value="3">Anulado</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-info btnEstado" data-idrenta="<?php echo $renta['id']; ?>">
                                                            Actualizar
                                                        </button>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>                               
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>