<?php if( !empty($vehiculos) ): ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Table With Full Features</h3>
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="tablaVehiculos" class="table table-bordered table-striped dataTable">
                                    <thead>
                                        <th>Id</th>
                                        <th>Placa</th>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Color</th>
                                        <th>Kilometraje</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach($vehiculos as $vehiculo): ?>
                                            <tr>
                                                <td><?php echo $vehiculo->id; ?></td>
                                                <td><?php echo $vehiculo->placa; ?></td>
                                                <td><?php echo $vehiculo->marca; ?></td>
                                                <td><?php echo $vehiculo->modelo; ?></td>
                                                <td><?php echo $vehiculo->color; ?></td>
                                                <td><?php echo $vehiculo->kilometraje.' km'; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>                               
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>