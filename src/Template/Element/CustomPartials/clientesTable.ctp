<?php if( !empty($clientes) ): ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Lista de Clientes</h3>
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="tablaClientes" class="table table-bordered table-striped dataTable">
                                    <thead>
                                        <th>Id</th>
                                        <th>Cedula</th>
                                        <th>Licencia</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Celular</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach($clientes as $cliente): ?>
                                            <tr>
                                                <td><?php echo $cliente->id; ?></td>
                                                <td><?php echo $cliente->cedula; ?></td>
                                                <td><?php echo $cliente->licencia; ?></td>
                                                <td><?php echo $cliente->nombre; ?></td>
                                                <td><?php echo $cliente->apellido; ?></td>
                                                <td><?php echo $cliente->celular; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>                               
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>