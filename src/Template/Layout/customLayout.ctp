<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Renta de Autos</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">        
        <link rel="stylesheet" href="/adminlte/css/AdminLTE.min.css">
        <link rel="stylesheet" href="/adminlte/css/skins/skin-blue.min.css">

        <link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">        
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">            
            <?= $this->element('CustomSection/header') ?>
            <?= $this->element('CustomSection/sidebar') ?>            
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Dashboard<small>Renta de Autos</small></h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                   <?= $this->fetch('content') ?>
                </section>
            </div>
            <?= $this->element('CustomSection/footer') ?>
        </div>
        <script src="/plugins/jQuery/jQuery-2.2.0.min.js"></script>
        <script src="/bootstrap/js/bootstrap.min.js"></script>

        <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>        
        <script src="/plugins/datepicker/bootstrap-datepicker.js"></script>

        <script src="/adminlte/js/app.min.js"></script>
        <script type="text/javascript">
            $('#tablaVehiculos, #tablaClientes').DataTable();
        </script>
        <script src="/js/main.js"></script>
    </body>
</html>