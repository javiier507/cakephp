<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Clientes Controller
 *
 * @property \App\Model\Table\ClientesTable $Clientes
 */
class ClientesController extends AppController
{
	public function index()
	{
		$clientes = $this->Clientes->find('all');
		$this->set(compact('clientes'));
	}

	public function registro()
	{
		$cliente = $this->Clientes->newEntity($this->request->data);
        if($this->request->is('post'))
        {
        	if($this->Clientes->save($cliente))
            {
                return $this->redirect('/clientes/index');
            }
            else
            {
            	$this->Flash->set('El Cliente no pudo ser registrado. Por favor, intente nuevamente.',
					['key' => 'error']);
            }
        }
	}
}
