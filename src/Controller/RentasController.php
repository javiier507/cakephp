<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Datasource\ConnectionManager;

/**
 * Rentas Controller
 *
 * @property \App\Model\Table\RentasTable $Rentas
 */
class RentasController extends AppController
{
	public function initialize()
	{
		$this->viewBuilder()->layout('customLayout');
		$this->loadModel('Vehiculos');
		$this->loadModel('Clientes');
		$this->connection = ConnectionManager::get('default'); 
	}

	public function getRentas()
	{
		$rentas = $this->connection->execute('select rentas.id, rentas.fecha_salida, rentas.fecha_entrada, rentas.costo, vehiculos.modelo as "modelo", clientes.cedula as "cedula", rentas.estado
			from rentas inner join vehiculos on rentas.id = vehiculos.id
			inner join clientes on rentas.id = clientes.id')->fetchAll('assoc');

		return $rentas;
	}

	public function index()
	{	
		$rentas = $this->getRentas();
		$this->set(compact('rentas'));
	}

	public function registro()
	{
		if( $this->request->is('get') )
		{
			$vehiculos = $this->connection->execute('select vehiculos.id, vehiculos.modelo
				from vehiculos left join rentas on vehiculos.id = rentas.vehiculo_id
				where rentas.estado is null or rentas.estado <> 1
				group by vehiculos.id, vehiculos.modelo')->fetchAll('assoc');
			

			$clientes = $this->Clientes->find('all', 
				['fields' => ['Clientes.id', 'Clientes.licencia', 
				'Clientes.nombre', 'Clientes.apellido'] ]);	
			
			$data = [compact('vehiculos'), compact('clientes')];			
			$this->set(compact('data'));
		}
		elseif($this->request->is('post'))
        {
        	$renta = $this->Rentas->newEntity($this->request->data);
        	if($this->Rentas->save($renta))
            {
                return $this->redirect('/rentas/index');
            }
            else
            {
            	$this->Flash->set('La renta no pudo ser registrada. Por favor, intente nuevamente.');
            }
        }
	}

	public function reporte()
	{
		$rentas = $this->getRentas();
		$this->set(compact('rentas'));
	}

	public function estado()
	{
		if( $this->request->is('post') )
		{
			$this->connection->update('rentas', ['estado' => $this->request->data['estado']], 
				['id' => $this->request->data['idrenta']]);

			echo json_encode( ['response' => 'success'] );
		}
	}
}
