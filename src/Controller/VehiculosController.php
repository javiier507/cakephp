<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Vechiculos Controller
 *
 * @property \App\Model\Table\VechiculosTable $Vechiculos
 */
class VehiculosController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

	public function index()
	{
		$vehiculos = $this->Vehiculos->find('all');
		$this->set(compact('vehiculos'));
	}

    public function modelos()
    {
        $vehiculos = $this->Vehiculos
            ->find()
            ->select(['id', 'modelo'])
            ->where(['marca =' => 'toyota'])
            ->order('id');

        echo json_encode($vehiculos);
    }

	public function registro()
	{
		if($this->request->is('get'))
        {
            $vehiculos = $this->Vehiculos
                ->find()
                ->select('marca')
                ->group('vehiculos.marca')
                ->order('id');

            $this->set(compact('vehiculos'));
        }
        elseif($this->request->is('post'))
        {
            $vehiculo = $this->Vehiculos->newEntity($this->request->data);
        	if($this->Vehiculos->save($vehiculo))
            {
                return $this->redirect('/');
            }
            else
            {
            	$this->Flash->set('El vehiculo no pudo ser registrado. Por favor, intente nuevamente.',
                    ['key' => 'error']);
            }
        }
	}
}
